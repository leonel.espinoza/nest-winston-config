export type { WinstonEnvironment } from './index.d'

export { NestWinstonSetup } from './config/winston'
export { StrictLogger } from './feature/strict-logger'
