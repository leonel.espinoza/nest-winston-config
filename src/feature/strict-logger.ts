import { Logger } from '@nestjs/common'

import type { GenerateLoggerDataProps, GenerateLoggerDataReturn, MessageDetails, StartTimerProps, StartTimerReturn } from './strict-logger.d'

const generateLoggerData = (props: GenerateLoggerDataProps): GenerateLoggerDataReturn => {
  const { ConstructorClass, ...baseInformation } = props

  return { ...baseInformation, context: ConstructorClass.name, value: null }
}

const log = (props: GenerateLoggerDataProps): void => {
  const data = generateLoggerData(props)

  Logger.log(data)
}

const warn = (props: GenerateLoggerDataProps<MessageDetails>): void => {
  const data = generateLoggerData(props)

  Logger.warn(data)
}

const error = (props: GenerateLoggerDataProps<MessageDetails>): void => {
  const data = generateLoggerData(props)

  Logger.error(data)
}

const startTimer = (WinstonLogger: StartTimerProps['WinstonLogger']): StartTimerReturn => {
  const timer = WinstonLogger.startTimer()

  return {
    endTimer: (props: GenerateLoggerDataProps) => {
      const data = generateLoggerData(props)
      timer.done(data)
    }
  }
}

export const StrictLogger = {
  log,
  warn,
  error,
  startTimer
}
