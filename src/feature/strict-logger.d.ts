import type { WinstonLogger } from '../config/winston.d'

export interface GenerateLoggerDataProps<TMessage = unknown> {
  ConstructorClass: (new () => unknown) | { name: string }
  id?: { variableName: string, value: string }
  contextType: string
  message: TMessage
}

export type GenerateLoggerDataReturn<TMessage = unknown> = Omit<GenerateLoggerDataProps<TMessage>, 'ConstructorClass'> | {
  context: string
  value: null
}

export interface MessageDetails {
  name: string
  description: string
  reasons: string | Array<string | undefined>
}

export interface StartTimerProps {
  WinstonLogger: WinstonLogger
}

export interface StartTimerReturn {
  endTimer: (props: GenerateLoggerDataProps) => void
}
