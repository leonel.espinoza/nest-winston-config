import { format, transports, createLogger, type LoggerOptions } from 'winston'
import { WinstonModule } from 'nest-winston'

import type { SetupProps, SetupReturn } from './winston.d'

const { combine, prettyPrint, timestamp, json, printf } = format

const winstonBaseConfig = {
  timestampFormat: timestamp({
    format: () =>
      new Date().toLocaleString('es-CL', {
        hour12: false,
        timeZone: 'America/Santiago'
      })
  })
}

const brConsoleFormat = (): ReturnType<typeof combine> => {
  const outputFormat = json({ replacer: (_, object) => { object.value = null } })
  return combine(
    winstonBaseConfig.timestampFormat,
    outputFormat,
    prettyPrint({ depth: 5 })
  )
}

const brEKSConsoleFormat = (): ReturnType<typeof combine> => {
  const outputFormat = printf((data) => JSON.stringify({ ...data, value: null }))
  return combine(winstonBaseConfig.timestampFormat, outputFormat)
}

export const NestWinstonSetup = ({ environment, appName, flow }: SetupProps): SetupReturn => {
  const IS_PROD = environment === 'production'
  const IS_LOCAL = environment === 'local'

  const loggerConfig: LoggerOptions = {
    level: IS_PROD ? 'info' : 'debug',
    format: IS_LOCAL ? brConsoleFormat() : brEKSConsoleFormat(),
    defaultMeta: { appName, flow },
    transports: [
      new transports.Console({
        format: format.colorize({ all: IS_LOCAL })
      })
    ]
  }

  return {
    WinstonLogger: createLogger(loggerConfig),
    WinstonLoggerModule: WinstonModule.createLogger(loggerConfig)
  }
}
