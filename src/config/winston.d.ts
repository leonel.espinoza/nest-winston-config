import type * as winston from 'winston'
import type { WinstonModule as IWinstonModule } from 'nest-winston'

export interface WinstonLogger extends winston.Logger {}
export interface WinstonModule extends ReturnType<typeof IWinstonModule.createLogger> {}
export type WinstonEnvironment = 'local' | 'development' | 'release' | 'production'

export interface SetupProps { environment: WinstonEnvironment, appName: string, flow: string }
export interface SetupReturn { WinstonLogger: WinstonLogger, WinstonLoggerModule: WinstonModule }
